import {Component, ViewEncapsulation, ViewChild, OnInit, Output, EventEmitter} from '@angular/core';
import {
  NbWindowService,
  NbDialogService,
  NbComponentStatus,
  NbGlobalPhysicalPosition,
  NbGlobalPosition,
  NbToastrService
} from '@nebular/theme';
import {HttpClient} from '@angular/common/http';
import 'rxjs/Rx';
import {DatePipe} from '@angular/common'
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import {generateordersdata} from './test.component';
import {HttpsService} from '../../../@theme/services/https.service';
import {AreasComponent, EditAreasComponent, DelAreasComponent} from '../../../@theme/directives';
import {ToastComponent} from '../../../@theme/directives/toast/toast.component';


export interface Config {
  heroesUrl: string;
  textfile: string;
}

@Component({
  encapsulation: ViewEncapsulation.None,
  selector: 'ngx-fiter-targets',
  styleUrls: ['./fiter_target.component.scss'],
  templateUrl: './fiter_target.component.html',
})

export class FiterTargetComponent extends ToastComponent implements OnInit {
  @Output() getTitle = new EventEmitter();

  private apiURL = 'https://api.unsplash.com/photos'
  private headers: any = new Headers({'Content-Type': 'application/json'});

  constructor(private windowService: NbWindowService,
              private http: HttpClient,
              private datepipe: DatePipe,
              public sanitizer: DomSanitizer,
              private httptest: HttpsService,
              private dialogService: NbDialogService,
              public toastrService: NbToastrService
  ) {
    super();
  }

  ngOnInit() {
    // demo phuong thuc get
    //this.httptest.httpGet(this.apiURL,this.data);
  }

  dataSearch: any = {}

  photos: any = {};
  datasearch: String;

  onKey(event: any) {
    // without type info
    this.datasearch = event.target.value.trim();
  }

  test1: any = {};
  dulieu: any[] = [
    {
      code: 1,
      name: 'Sửa đơn vị',
    },
    {
      code: 2,
      name: 'Xoá đơn vị',
    }
  ]
  values = '';
  disabled = true;
  icon = 'calendar';
  icon1 = 'search-outline';
  tabs: any[] = [
    {
      title: 'Kinh doanh',
      route: 'tab1',
    },
    {
      title: 'Kênh',
      route: 'tab2',
    },
    {
      title: 'Sản phẩm',
      route: 'tab3',
    },
  ];

  list_chart = [{
    code: 1,
    name: 'Pie chart',
    imege: '../../../../assets/images/u729.png'
  },
    {
      code: 2,
      name: 'Pie chart',
      imege: '../../../../assets/images/u730.png'
    },
    {
      code: 3,
      name: 'Bar chart',
      imege: '../../../../assets/images/u731.png'
    },
  ];

  selected() {
    console.log("S");
  }

  data: any = {};

  getOrders() {
    return [
      {id: '1', name: 'order 1'},
      {id: '2', name: 'order 2'},
      {id: '3', name: 'order 3'},
      {id: '4', name: 'order 4'},
    ];
  }

  quotes = [
    {title: "test", body: 'We rock at Angular'},
    {title: null, body: 'Titles are not always needed'},
    {title: null, body: 'Toastr rock!'},
  ];
  types: NbComponentStatus[] = [
    'primary',
    'success',
    'info',
    'warning',
    'danger',
  ];

  openRandomToast() {
    super.showToast('primary', "quote.title", "quote.body");

  }


  positions: any[] = [
    {
      code: 1,
      name: 'Theo giá trị',
    },
    {
      code: 2,
      name: 'Theo thời gian',
    }
  ];
  department: any = {};
  departments: any[] = [
    {
      code: 1,
      name: 'Nhóm chỉ tiêu VTT',
    },
    {
      code: 2,
      name: 'Kinh Doanh',
    },
    {
      code: 3,
      name: 'Kỹ thuật',
    },
    {
      code: 4,
      name: 'Đầu tư tài chính',
    },
    {
      code: 5,
      name: 'Tổ chức nhân lực',
    },
    {
      code: 6,
      name: 'Tài chính kế toán',
    }
  ];

  selectValue() {
    this.dataSearch.donvi = this.data.code;
    this.dataSearch.baocao = this.department.code;
    console.log(this.data);
    console.log(this.department);
    console.log(this.dataSearch);
  }

  te: string = "";
  isshow: boolean = true;
  chart: any = {};

  onSelect(item) {

    this.chart = item;
    this.isshow = !this.isshow;
    console.log(this.isshow);
  }

  openWindowSearch(contentSearchTemplate) {
    console.log(this.data);
    if (this.data.code === 1) {
      this.windowService.open(
        contentSearchTemplate,
        {
          title: 'Tạo biểu đồ chỉ tiêu',
          context: {
            text: 'some text to pass into template',
          },
        },
      );
    }

  }

  openWindow(contentTemplate) {
    this.windowService.open(
      contentTemplate,
      {
        title: 'Quản lý đơn vị',
        context: {
          text: 'some text to pass into template',
        },
      },
    );
  }

  onEventStartEndRange(event) {
    if (event != null) {
      this.dataSearch.time = this.datepipe.transform(event, 'yyyy-MM-dd');
    } else {
    }
  }

  onClickEditDel(item) {
    if (item.code === 1) {
      this.dialogService.open(EditAreasComponent);
    } else if (item.code === 2) {
      this.dialogService.open(DelAreasComponent);
    }
  }

  open() {
    this.dialogService.open(AreasComponent, {
      context: {
        title: 'Sửa đơn vị',
        data: { code: '123', name: '123', parent: '123132',
          startDate: null, endDate: null },
      },
    });

  }
}
