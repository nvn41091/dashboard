import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  encapsulation: ViewEncapsulation.None,
  selector: 'ngx-fiter-target',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class SearchTargetComponent {

}
