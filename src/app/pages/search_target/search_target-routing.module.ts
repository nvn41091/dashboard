import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SearchTargetComponent } from './search_target.component';
import {FiterTargetComponent} from './fiter_target/fiter_target.component';
import { KenhTargetComponent } from './fiter_target/Kenh.component';
import { ProductsTargetComponent } from './fiter_target/Products_target.component';
import { KinhDoanhTargetComponent } from './fiter_target/Kinh-doanh.component';


const routes: Routes = [{
  path: '',
  component: SearchTargetComponent,
  children: [
    {
      path: '',
      component: FiterTargetComponent,
      children: [
        {
          path: 'tab1',
          component: KinhDoanhTargetComponent,
        },
        {
          path: 'tab2',
          component: KenhTargetComponent,
        },
        {
          path: 'tab3',
          component: ProductsTargetComponent,
        },

      ],

    },

  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SearchTargetRoutingModule { }

export const routedComponents = [
  SearchTargetComponent,
];
