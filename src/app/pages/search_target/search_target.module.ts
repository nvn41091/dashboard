import {NgModule} from '@angular/core';
import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule, NbCheckboxModule,
  NbDatepickerModule, NbIconModule,
  NbInputModule, NbRadioModule, NbSelectModule, NbTreeGridModule,
  NbUserModule,
  NbTabsetModule,
  NbRouteTabsetModule,
  NbFormFieldModule,
} from '@nebular/theme';

import {ThemeModule} from '../../@theme/theme.module';
import {SearchTargetRoutingModule, routedComponents} from './search_target-routing.module';
import {FiterTargetComponent} from './fiter_target/fiter_target.component';
import {ChartsModule} from '../charts/charts.module';
import {FormsRoutingModule} from '../forms/forms-routing.module';
import {FormsModule as ngFormsModule} from '@angular/forms';
import {TablesModule} from '../tables/tables.module';
import { TreeViewModule } from '@progress/kendo-angular-treeview';
import { ReactiveFormsModule } from '@angular/forms';
import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule} from '@angular/material/button';
import { HttpsService } from '../../@theme/services/https.service';
import { NbDateFnsDateModule } from '@nebular/date-fns';
import { vi } from 'date-fns/locale';
import {
  TreeviewModule,
} from 'ngx-treeview';

@NgModule({
  imports: [
    MatButtonModule,
    MatMenuModule,
    ReactiveFormsModule,
    NbRouteTabsetModule,
    NbTabsetModule,
    NbCardModule,
    ThemeModule,
    NbDatepickerModule,
    SearchTargetRoutingModule,
    NbInputModule,
    NbCardModule,
    NbButtonModule,
    NbActionsModule,
    NbUserModule,
    NbCheckboxModule,
    NbRadioModule,
    FormsRoutingModule,
    NbSelectModule,
    NbIconModule,
    ngFormsModule,
    ChartsModule,
    NbTreeGridModule,
    TablesModule,
    TreeViewModule,
    NbFormFieldModule,
    NbDateFnsDateModule.forRoot({
      parseOptions: { locale: vi },
      formatOptions: { locale: vi },
    }),
    TreeviewModule.forRoot(),
  ],
  declarations: [
    ...routedComponents,
    FiterTargetComponent,
  ],
  providers: [
    HttpsService,
    ],
})
export class SearchTargetModule {
}
