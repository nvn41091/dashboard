import {Component} from '@angular/core';
import {EchartsPieComponent} from './echarts-pie.component';
import {EchartsBarComponent} from './echarts-bar.component';
import {EchartsLineComponent} from './echarts-line.component';
import {EchartsMultipleXaxisComponent} from './echarts-multiple-xaxis.component';
import {EchartsAreaStackComponent} from './echarts-area-stack.component';
import {EchartsBarAnimationComponent} from './echarts-bar-animation.component';
import {EchartsRadarComponent} from './echarts-radar.component';

@Component({
  selector: 'ngx-echarts',
  styleUrls: ['./echarts.component.scss'],
  templateUrl: './echarts.component.html',
})
export class EchartsComponent {
  charts = [
    {
      title: 'Pie',
      component: EchartsPieComponent,
    },
    {
      title: 'Line',
      component: EchartsLineComponent,
    },
    {
      title: 'Area Stack',
      component: EchartsAreaStackComponent,
    },
    {
      title: 'Radar',
      component: EchartsRadarComponent,
    },
  ];
  charts1 = [
    {
      title: 'Bar',
      component: EchartsBarComponent,
    },
    {
      title: 'Multiple x-axis',
      component: EchartsMultipleXaxisComponent,
    },
    {
      title: 'Bar Animation',
      component: EchartsBarAnimationComponent,
    },
  ];
}
