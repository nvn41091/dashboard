import { Component } from '@angular/core';
import { NbDateService } from '@nebular/theme';
import { DatePipe } from '@angular/common';


@Component({
  selector: 'ngx-datepicker',
  templateUrl: 'datepicker.component.html',
  styleUrls: ['datepicker.component.scss'],
})
export class DatepickerComponent {

  min: Date;
  max: Date;
  format_date: Date;
  date: string;
  test1: Date;

  constructor(protected dateService: NbDateService<Date>) {
    this.min = this.dateService.addDay(this.dateService.today(), -5);
    this.max = this.dateService.addDay(this.dateService.today(), 5);
    // this.date = this.dateService.format(this.format_date,'dd/MM/yyyy');

  }
  // test(){
  //   // console.log(this.date);
  //   // console.log(this.format_date);
  //   const dateSendingToServer = new DatePipe('en-US').transform(this.test1, 'dd/MM/yyyy');
  //   // console.log(dateSendingToServer);
  // }
}
