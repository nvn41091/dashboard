import { Component } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';

import { SmartTableData } from '../../../@core/data/smart-table';
function genData(items, parent,itemArr):any[] {
  debugger;

               for (let i = 0; i < items.length; i++) {
                   if (items[i].parentid === parent) {
                       let row = items[i];
                       row.id = items[i].id;
                       row.text = items[i].text;
                       row.items = genData(items, items[i].id,itemArr);
                       itemArr.push(row);
                   }
               }

              return itemArr;

           }

 function generateordersdata(): any[] {
  let data1 = [
       { id:1, parentid:0, name:"Order1", date:"ASD"  },
       { id:2, parentid:1, name:"Order1.1",date:"date" },
       { id:3, parentid:2, name:"Order1.3",date:"date" },
       { id:4, parentid:0, name:"Order2",date:"date" },
       { id:5, parentid:0, name:"Order3",date:"date" },
       { id:6, parentid:9, name:"Order4",date:"date" },
       { id:7, parentid:1, name:"Order1.1",date:"date" },
       { id:8, parentid:1, name:"Order1.1",date:"date" },
       { id:9, parentid:0, name:"Order5",date:"date" },
       { id:10, parentid:0, name:"Order5",date:"date" },


   ]

    return genData(data1,0,new Array());

}

@Component({
  selector: 'ngx-smart-table',
  templateUrl: './smart-table.component.html',
  styleUrls: ['./smart-table.component.scss'],
})
export class SmartTableComponent {

  settings = {

    columns: {
      id: {
        title: 'ID',
        type: 'number',
      },
      firstName: {
        title: 'First Name',
        type: 'string',
      },
      lastName: {
        title: 'Last Name',
        type: 'string',
      },
      username: {
        title: 'Username',
        type: 'string',
      },
      email: {
        title: 'E-mail',
        type: 'string',
      },
      age: {
        title: 'Age',
        type: 'number',
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();
  // source.data - generateordersdata();
  constructor(private service: SmartTableData) {
    const data = generateordersdata();
    this.source.load(data);
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }
}
