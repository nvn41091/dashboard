import { NgModule } from '@angular/core';
import { NbCardModule, NbIconModule, NbInputModule, NbTreeGridModule } from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { ThemeModule } from '../../@theme/theme.module';
import { TablesRoutingModule, routedComponents } from './tables-routing.module';
import {FsIconComponent, TreeGridComponent} from './tree-grid/tree-grid.component';
import {TreeUnitComponent} from './tree-unit/tree-unit.component';
import {MatTreeModule} from '@angular/material/tree';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatBottomSheetModule} from '@angular/material/bottom-sheet';
import {MatInputModule} from '@angular/material/input';
import { TreeViewModule } from '@progress/kendo-angular-treeview';
import { SmartTableComponent } from './smart-table/smart-table.component';

@NgModule({
  imports: [
    NbCardModule,
    NbTreeGridModule,
    NbIconModule,
    NbInputModule,
    ThemeModule,
    TablesRoutingModule,
    Ng2SmartTableModule,
    MatTreeModule,
    MatIconModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatIconModule,
    MatInputModule,
    TreeViewModule,
  ],
  declarations: [
    ...routedComponents,
    FsIconComponent,
  ],
  exports: [
    TreeGridComponent,
    TreeUnitComponent,
    SmartTableComponent,
  ],
})
export class TablesModule { }
