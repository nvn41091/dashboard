import { Component, Input } from '@angular/core';
import { NbSortDirection, NbSortRequest, NbTreeGridDataSource, NbTreeGridDataSourceBuilder } from '@nebular/theme';


function genData(items, id,itemArr):any[] {
  let dulieu = {};
  let data ={childrens1:[]};
  let childrens = [];
  let itemArrchildrens1 = [];
               for (let i = 0; i < items.length; i++) {
                   if (items[i].parentid === id) {
                       let row = items[i];
                       row.id = items[i].id;
                       row.text = items[i].text;
                       dulieu = row;
                       row.items = genData(items, items[i].id,itemArr);

                       itemArr.push(row);
                       data = { childrens1: itemArr};
                   }
               }
               console.log(itemArr);
               console.log(data);
              console.log(dulieu);
              data: {

              }
              //  0: {id: 3, parentid: 2, name: "Order1.3", date: "date", text: undefined, …}
              //  1: {id: 2, parentid: 1, name: "Order1.1", date: "date", text: undefined, …}
              //  2: {id: 7, parentid: 1, name: "Order1.1", date: "date", text: undefined, …}
              //  3: {id: 8, parentid: 1, name: "Order1.1", date: "date", text: undefined, …}
              //  4: {id: 1, parentid: 0, name: "Order1", date: "ASD", text: undefined, …}
              //  5: {id: 4, parentid: 0, name: "Order2", date: "date", text: undefined, …}
              //  6: {id: 5, parentid: 0, name: "Order3", date: "date", text: undefined, …}
              //  7: {id: 6, parentid: 9, name: "Order4", date: "date", text: undefined, …}
              //  8: {id: 9, parentid: 0, name: "Order5", date: "date", text: undefined, …}
              //  9: {id: 10, parentid: 0, name: "Order5", date: "date", text: undefined, …}
              return itemArr;

           }
           function getNestedChildren(models, parentId) {
            //  let datatest ={};
            const nestedTreeStructure = [];
            const length = models.length;

            for (let i = 0; i < length; i++) { // for-loop for perf reasons, huge difference in ie11
                const model = models[i];
                let data ={data:{id:null, parentid:null},children:[]};
                model.datatest =models[i].id;
                data.data.id = models[i].id;
                data.data.parentid = models[i].parentid;
                if (model.parentid == parentId) {
                    const children = getNestedChildren(models, model.id);

                    if (children.length > 0) {
                        data.children = children;
                    }

                    nestedTreeStructure.push(data);
                }
            }

            return nestedTreeStructure;
        }
 function generateordersdata(): any[] {
  let data1 = [
       { id:1, parentid:0, name:"Order1", date:"ASD"  },
       { id:2, parentid:1, name:"Order1.1",date:"date" },
       { id:3, parentid:2, name:"Order1.3",date:"date" },
       { id:4, parentid:0, name:"Order2",date:"date" },
       { id:5, parentid:0, name:"Order3",date:"date" },
       { id:6, parentid:9, name:"Order4",date:"date" },
       { id:7, parentid:1, name:"Order1.1",date:"date" },
       { id:8, parentid:1, name:"Order1.1",date:"date" },
       { id:9, parentid:0, name:"Order5",date:"date" },
       { id:10, parentid:0, name:"Order5",date:"date" },
   ]
    return getNestedChildren(data1,0);

}




@Component({
  selector: 'ngx-tree-grid',
  templateUrl: './tree-grid.component.html',
  styleUrls: ['./tree-grid.component.scss'],
})

export class TreeGridComponent {
  debugger;
   data1 = [
    { id:1, parentid:0, name:"Order1", date:"ASD"  },
    { id:2, parentid:1, name:"Order1.1",date:"date" },
    { id:3, parentid:2, name:"Order1.3",date:"date" },
    { id:4, parentid:0, name:"Order2",date:"date" },
    { id:5, parentid:0, name:"Order3",date:"date" },
    { id:6, parentid:9, name:"Order4",date:"date" },
    { id:7, parentid:1, name:"Order1.1",date:"date" },
    { id:8, parentid:1, name:"Order1.1",date:"date" },
    { id:9, parentid:0, name:"Order5",date:"date" },
    { id:10, parentid:0, name:"Order5",date:"date" },
];

  name: string;
  date: string;
  customColumn = 'name';
  defaultColumns = [ 'date'];
  allColumns = [ this.customColumn, ...this.defaultColumns ];

  dataSource:any = generateordersdata();
  getShowOn(index: number) {
    const minWithForMultipleColumns = 400;
    const nextColumnStep = 100;
    return minWithForMultipleColumns + (nextColumnStep * index);
  }
}

@Component({
  selector: 'ngx-fs-icon',
  template: `
    <nb-tree-grid-row-toggle [expanded]="expanded" *ngIf="isDir(); else fileIcon">
    </nb-tree-grid-row-toggle>
    <ng-template #fileIcon>
      <nb-icon icon="file-text-outline"></nb-icon>
    </ng-template>
  `,
})
export class FsIconComponent {
  @Input() kind: string;
  @Input() expanded: boolean;

  isDir(): boolean {
    return this.kind === 'dir';
  }
}
