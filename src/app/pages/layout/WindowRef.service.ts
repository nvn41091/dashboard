import { Injectable } from '@angular/core';
import { NbWindowRef } from '@nebular/theme';

@Injectable()
export class WindowRef {

  get nativeWindow(): any {
    return _window();
  }
  get tat() {
    return close();
  }

}

function close() {
  console.log(window);
    if (this._closed) {
      return;
    }

    this._closed = true;
    this.componentRef.destroy();
    this.stateChange$.complete();
    this.closed$.next();
    this.closed$.complete();
  }


function _window(): any {
  // return the native window obj
  return window;
}
