import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
  NbActionsModule,
  NbLayoutModule,
  NbMenuModule,
  NbSearchModule,
  NbSidebarModule,
  NbUserModule,
  NbContextMenuModule,
  NbButtonModule,
  NbSelectModule,
  NbIconModule,
  NbThemeModule,
  NbTabsetModule,
  NbCardModule,
  NbDatepickerModule,
  NbInputModule,
  NbRouteTabsetModule,
  NbCheckboxModule,
  NbRadioModule,
  NbTreeGridModule,
  NbDialogModule,
  NbWindowModule,
  NbPopoverModule,
  NbTooltipModule,
  NbFormFieldModule,
  NbCalendarModule,
} from '@nebular/theme';
import {NbEvaIconsModule} from '@nebular/eva-icons';
import {NbSecurityModule} from '@nebular/security';

import {
  FooterComponent,
  HeaderComponent,
  SearchInputComponent,
  TinyMCEComponent,
} from './components';
import {
  CapitalizePipe,
  PluralPipe,
  RoundPipe,
  TimingPipe,
  NumberWithCommasPipe,
} from './pipes';
import {
  OneColumnLayoutComponent,
  ThreeColumnsLayoutComponent,
  TwoColumnsLayoutComponent,
} from './layouts';
import {AutoSearchComponent,PopupComponent, AreasComponent,
  EditAreasComponent, DelAreasComponent, MustMatchDirective} from './directives';

import {DEFAULT_THEME} from './styles/theme.default';
import {COSMIC_THEME} from './styles/theme.cosmic';
import {CORPORATE_THEME} from './styles/theme.corporate';
import {DARK_THEME} from './styles/theme.dark';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {FormsModule as ngFormsModule} from '@angular/forms';
import { ToastComponent } from './directives/toast/toast.component';
import {TreeviewModule} from "ngx-treeview";

const NB_MODULES = [
  NbCalendarModule,
  ngFormsModule,FormsModule,
  NbDialogModule.forChild(),
  NbLayoutModule,
  NbMenuModule,
  NbSearchModule,
  NbSidebarModule,
  NbContextMenuModule,
  NbSecurityModule,
  NbButtonModule,
  NbSelectModule,
  NbIconModule,
  NbEvaIconsModule,
  NbRouteTabsetModule,
  NbTabsetModule,
  NbCardModule,
  NbDatepickerModule,
  NbInputModule,
  NbActionsModule,
  NbUserModule,
  NbCheckboxModule,
  NbRadioModule,
  NbTreeGridModule,
  NbPopoverModule,
  NbTooltipModule,
];
const COMPONENTS = [
  ToastComponent,
  DelAreasComponent,
  EditAreasComponent,
  AreasComponent,
  HeaderComponent,
  FooterComponent,
  SearchInputComponent,
  TinyMCEComponent,
  OneColumnLayoutComponent,
  ThreeColumnsLayoutComponent,
  TwoColumnsLayoutComponent,
];
const PIPES = [
  CapitalizePipe,
  PluralPipe,
  RoundPipe,
  TimingPipe,
  NumberWithCommasPipe,
];
const DIRECTIVER = [
  ToastComponent,
  DelAreasComponent,
  EditAreasComponent,
  AreasComponent,
  PopupComponent,
  AutoSearchComponent,
];

@NgModule({
  imports: [NbFormFieldModule, CommonModule, ...NB_MODULES, FormsModule, ReactiveFormsModule, TreeviewModule],
  exports: [CommonModule, ...PIPES, ...COMPONENTS, ...DIRECTIVER],
  declarations: [...COMPONENTS, ...PIPES, ...DIRECTIVER,  MustMatchDirective  ],
})
export class ThemeModule {
  static forRoot(): ModuleWithProviders<ThemeModule> {
    return {
      ngModule: ThemeModule,
      providers: [
        ...NbThemeModule.forRoot(
          {
            name: 'default',
          },
          [DEFAULT_THEME, COSMIC_THEME, CORPORATE_THEME, DARK_THEME],
        ).providers,
      ],
    };
  }
}
