export * from '../components/header/header.component';
export * from '../components/footer/footer.component';
export * from '../components/search-input/search-input.component';
export * from '../components/tiny-mce/tiny-mce.component';
