import { Component } from '@angular/core';

@Component({
  selector: 'ngx-footer',
  styleUrls: ['./footer.component.scss'],
  template: `
    <span class="created-by" >
    Create by Viettel
    </span>
  `,
})
export class FooterComponent {
}
