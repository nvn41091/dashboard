import {Component, Input} from '@angular/core';
import {NbDialogRef} from '@nebular/theme';
import {AreasComponent} from './areas/addAreas/areas.component';
import {DelAreasComponent} from '..';
import {HttpsService} from '../../services/https.service';
import {DatePipe} from '@angular/common';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'ngx-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.scss'],
})

export class PopupComponent {
  @Input() title: any;

  [x: string]: any;

  names: any;
  @Input() popUp: FormGroup;

  constructor(protected ref: NbDialogRef<DelAreasComponent>,
              private http: HttpsService,
              private datepipe: DatePipe,
              private fb: FormBuilder) {
    this.createForm();
  }

  areasAdd: any = {
    codeAreas: null,
    nameAreas: null,
    parenAreas: null,
  };
  datasearch: any = {};


  cancel() {
    this.ref.close();
  }

  submit() {
    this.cancel();
  }

  open() {
    this.dialogService.open(AreasComponent);
  }

  createForm() {
    this.popUp = this.fb.group({
      codeAreas: ['', Validators.required],
      nameAreas: ['', Validators.required],
    });
  }
}
