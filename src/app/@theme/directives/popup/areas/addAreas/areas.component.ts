import {Component, NgModule, Input, Directive} from '@angular/core';
import {NbDialogRef, NbDateService, NbToastrService} from '@nebular/theme';
import {HttpsService} from '../../../../services/https.service';
import {DatePipe} from '@angular/common';
import {
    FormGroup,
    FormBuilder,
    Validators,
    NG_VALIDATORS,
    Validator,
    ValidationErrors, FormControl,
} from '@angular/forms';
import {TreeviewConfig, TreeviewItem} from 'ngx-treeview';

export function MustMatch(startDateControl: any, endDateControl: any,
                          nameControl: any, codeControl: any) {
    return (formGroup: FormGroup) => {
        const startDate = formGroup.controls[startDateControl];
        const endDate = formGroup.controls[endDateControl];
        const name = formGroup.controls[nameControl];
        const code = formGroup.controls[codeControl];

        if (!startDate || !endDate || !name || !code || !parent) {
            return null;
        }
        if (isEmpty(name.value)) {
            name.setErrors( {required: true} );
        }
        if ( isEmpty(code.value) ) {
            code.setErrors( {required: true});
        }
        if (name.value.length > 100) {
            name.setErrors({maxlength: true});
        }
        const regex = /<[^>]*>/;
        if (regex.test(name.value)) {
            name.setErrors({errorHtml: true});
        }
        if (code.value.length > 50) {
            code.setErrors({maxlength: true});
        }
        if (regex.test(code.value)) {
            code.setErrors({errorHtml: true});
        }
        const regDate = /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[13-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4((?:19|20)\d{2})$/;
        if (!isEmpty(startDate.value)) {
            if (!regDate.test(startDate.value)) {
                startDate.setErrors({invalidDate: true});
            }
        }
        if (!isEmpty(endDate.value)) {
            if (!regDate.test(endDate.value)) {
                endDate.setErrors({invalidDate: true});
            }
        }
        if (!isEmpty(startDate.value) && !isEmpty(endDate.value)) {
        }
        console.log(formGroup.valid);
    };
}

function isEmpty(val) {
    return (val === undefined || val == null || val.length <= 0) ? true : false;
}

function stringToDate(val) {
    const arrDate = val.split('/');
    const stringDate = arrDate[2] + '-' + arrDate[1] + '-' + arrDate[0];
    return new Date(stringDate);
}

@Directive({
    selector: '[mustMatch]',
    providers: [{provide: NG_VALIDATORS, useExisting: MustMatchDirective, multi: true}],
})
export class MustMatchDirective implements Validator {
    @Input('mustMatch') mustMatch: string[] = [];

    validate(formGroup: FormGroup): ValidationErrors {
        return MustMatch(this.mustMatch[0], this.mustMatch[1],
            this.mustMatch[2], this.mustMatch[3])(formGroup);
    }
}

@Component({
    selector: 'ngx-areas',
    templateUrl: './areas.component.html',
    styleUrls: ['./areas.component.scss'],
})

export class AreasComponent {
    data: AreaDTO = {code: '', name: '', parent: '', startDate: null, endDate: null};
    title: string;
    date1: string;
    date2: string;

    constructor(public ref: NbDialogRef<AreasComponent>,
                private http: HttpsService,
                private datepipe: DatePipe,
                private fb: FormBuilder,
                private toastrService: NbToastrService) {
    }

    config =  TreeviewConfig.create({
        hasAllCheckBox: true,
        hasFilter: true,
        hasCollapseExpand: true,
        decoupleChildFromParent: false,
        maxHeight: 400,
    });
    items = new TreeviewItem({
        text: 'IT', value: 9, children: [
            {
                text: 'Programming', value: 91, children: [{
                    text: 'Frontend', value: 911, children: [
                        { text: 'Angular 1', value: 9111 },
                        { text: 'Angular 2', value: 9112 },
                        { text: 'ReactJS', value: 9113 },
                    ],
                }, {
                    text: 'Backend', value: 912, children: [
                        { text: 'C#', value: 9121 },
                        { text: 'Java', value: 9122 },
                        { text: 'Python', value: 9123, checked: false },
                    ],
                }],
            },
            {
                text: 'Networking', value: 92, children: [
                    { text: 'Internet', value: 921 },
                    { text: 'Security', value: 922 },
                ],
            },
        ],
    });

    onSelectedChange(e) {
        console.log(e);
    }

    changeModelDate(isStartDate: boolean) {
        try {
            const reg = /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[13-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4((?:19|20)\d{2})$/;
            if (isStartDate) {
                if (reg.test(this.date1)) {
                    const dateFormat = stringToDate(this.date1);
                    if (!isNaN(dateFormat.getTime())) {
                        this.data.startDate = dateFormat;
                    }
                } else {
                    this.data.startDate = null;
                }
            } else {
                if (reg.test(this.date2)) {
                    const dateFormat = stringToDate(this.date2);
                    if (!isNaN(dateFormat.getTime())) {
                        this.data.endDate = dateFormat;
                    }
                } else {
                    this.data.endDate = null;
                }
            }
        } catch (e) {
        }
    }

    changeDate(isStartDate: boolean) {
        if (isStartDate) {
            this.date1 = this.datepipe.transform(this.data.startDate, 'dd/MM/yyyy');
        } else {
            this.date2 = this.datepipe.transform(this.data.endDate, 'dd/MM/yyyy');
        }
    }

    cancel() {
        this.ref.close();
    }


    submit() {
        console.log('done');
    }


    // form: FormGroup;
    //
    // createForm() {
    //     this.form = this.fb.group({
    //         codeAreas: new FormControl('', [Validators.required]),
    //         nameAreas: new FormControl('', [Validators.required]),
    //     });
    // }
}

export interface AreaDTO {
    code: string;
    name: string;
    parent: string;
    startDate: Date;
    endDate: Date;
}
