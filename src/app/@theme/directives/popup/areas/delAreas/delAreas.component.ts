import { Component, NgModule, Input } from '@angular/core';
import { NbDialogRef} from '@nebular/theme';
import { HttpsService } from '../../../../services/https.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'ngx-del-areas',
  templateUrl: './delAreas.component.html',
  styleUrls: ['./delAreas.component.scss'],
})

export class DelAreasComponent {
  @Input() title: string;

  constructor(
    protected ref: NbDialogRef<DelAreasComponent>,
    private http: HttpsService,
    private datepipe: DatePipe,
    ) {}

    areasAdd : any = {
      codeAreas:null,
      nameAreas:null,
      parenAreas:null
    };
    datasearch: any = {}


  cancel() {
    this.ref.close();
  }

  submit() {
    debugger;
    this.http.httpPost("url",this.areasAdd);
    this.cancel();
  }

  onEventStartEndRange(event)
  {

    if (event != null) {
      this.datasearch.time = this.datepipe.transform(event, 'yyyy-MM-dd');
    } else {
      console.log("date dang null")
    }
  }


}
