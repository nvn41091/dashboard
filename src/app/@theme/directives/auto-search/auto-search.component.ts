import { Component } from '@angular/core';
import {NbInputDirective, NbWindowService} from '@nebular/theme';

@Component({
  selector: 'ngx-auto-search',
  templateUrl: './auto-search.component.html',
  styleUrls: ['./auto-search.component.scss'],
})
export class AutoSearchComponent {
  values = '';
  disabled = true;
  icon = 'search-outline';
  constructor(private windowService: NbWindowService) {
  }



  onKey(event: any) { // without type info
    this.values = event.target.value.trim();
  }
}
