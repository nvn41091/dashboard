import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';



@Injectable()
export class HttpsService {

  private headers: any = new Headers({'Content-Type': 'application/json'});

  constructor(private http: HttpClient) {}


  // phuong thuc get bat buoc nhap data kieu string
  // parse: đổi json thành đối tượng json
  // stringify: đổi đối tượng js thành dạng json
  httpGet(link:string,data:any) {
    return this.http.get(link,
      {
        headers: this.headers,
        params: JSON.parse(data)
      }
      ).toPromise()
      .then(res => res)
      .catch(this.handleError);
  }

  httpPost(link:string,data:any){
    return this.http.post(link,
      JSON.stringify(data),
      {headers: this.headers}
    )
      .toPromise()
      .then(res => res)
      .catch(this.handleError);
  }

  httpDelete(link:string,data:any){
    return this.http.delete(link,
      {
        headers: this.headers,
        params: JSON.parse(data)
      }
    )
      .toPromise()
      .then(res => res)
      .catch(this.handleError);
  }
  httpPut(link:string,data:any){
    return this.http.put(link,
      {
        headers: this.headers,
        params: JSON.parse(data)
      }
    )
      .toPromise()
      .then(res => res)
      .catch(this.handleError);
  }


  private handleError(error:any):
  Promise < any > {
    return Promise.reject(error.message || error);
}

}
