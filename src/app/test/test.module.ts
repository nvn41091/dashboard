import { NgModule } from '@angular/core';
import { NbMenuModule } from '@nebular/theme';

import { ThemeModule } from '../@theme/theme.module';
import { TestComponent } from './test.component';
import { TestRoutingModule } from './test-routing.module';

@NgModule({
  imports: [
    TestRoutingModule,
    ThemeModule,
  ],
  declarations: [
    TestComponent,
  ],
})
export class TestModule {
}
